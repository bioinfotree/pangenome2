# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

MAKE := bmake

ref.fa:
	ln -sf $(REF) $@

genome.tab: ref.fa
	fasta_length <$< >$@

rnaseq.fstats rnaseq.cov annot.gff3.gz scapa.bed scaff.fa:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell zcat <$(call get,$(SAMPLE),SCAFF) >scaff.fa) \
		$(shell ln -sf $(call get,$(SAMPLE),SCAPA) scapa.bed) \
		$(shell ln -sf $(call get,$(SAMPLE),GENE) annot.gff3.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),RNASEQ_COV) rnaseq.cov) \
		$(shell ln -sf $(call get,$(SAMPLE),RNASEQ_FSTATS) rnaseq.fstats) \
	)
	sleep 3

# # divide the referencce into windows of 200kb
# ref.windows.bed: genome.tab
	# $(call load_modules); \
	# bedtools makewindows -g $< -w $(WIN_SIZE) -i srcwinnum >$@

# sort and convert one time for performances
annot.sort.bed: annot.gff3.gz
	$(call load_modules);
	zcat $< \
	| gff2bed --max-mem 10G \
	| sortBed -i >$@


# scaff.fa.fai ref.fa.fai need by each sub calculation
%.fa.fai: %.fa
	$(call load_modules);
	samtools faidx $<


# Prepare directories and generate directories list
dir_lst.mk: makefile ref.fa annot.sort.bed scapa.bed scaff.fa genome.tab rnaseq.cov scaff.fa.fai ref.fa.fai
	$(call load_modules);
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_1.1.mk";
	if [ -s $$RULES_PATH ] && [ -s $< ]; then
		printf "DIR_LST :=" >$@;
		FASTA_LST=$$(fasta2tab <$^2 | tr ' ' \\t | cut -f1);
		for SEQ in $$FASTA_LST; do
			FILENAME=$${SEQ%.*};
			mkdir -p $$FILENAME;
			cd $$FILENAME;
			ln -sf ../$<;
			ln -sf $$RULES_PATH rules.mk;
			ln -sf ../$^2;
			ln -sf ../$^3;
			bawk -v chr="$$FILENAME" '{ if ( $$1 == chr ) print $$0  }' <../$^4 >$^4;
			ln -sf ../$^5;
			bawk -v chr="$$FILENAME" '{ if ( $$1 == chr ) print $$0  }' <../$^6 >$^6;
			ln -sf ../$^7;
			ln -sf ../$^8;
			ln -sf ../$^9;
			cd ..;
			printf " $$FILENAME" >>$@;
		done;
	fi

include dir_lst.mk


# execute bmake on each subdirectory
subsystem.flag: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do
	  if [ -d $$D ]; then
	    cd $$D;
	    $(MAKE);
	    cd ..;
	  fi;
	done \
	&& touch $@


# get all generated novel exons
exon.novel.win.bed: subsystem.flag
	>$@;
	for D in $(DIR_LST); do
	  if [ -s $$D/$@ ]; then
	    cd $$D;
	    bawk -v dir="$$D" '!/^[\#+,$$]/ {$$4=dir; print}' <$@ >>../$@;
	    cd ..;
	  fi;
	done;

# get all generated novel exons
exon.tot.win.bed: subsystem.flag
	>$@;
	for D in $(DIR_LST); do
	  if [ -s $$D/$@ ]; then
	    cd $$D;
	    bawk -v dir="$$D" '!/^[\#+,$$]/ {$$4=dir; print}' <$@ >>../$@;
	    cd ..;
	  fi;
	done;


# get all generated novel exons
exon.tot.scaff.bed: subsystem.flag
	>$@;
	for D in $(DIR_LST); do
	  if [ -s $$D/$@ ]; then
	    cd $$D;
	    bawk -v dir="$$D" '!/^[\#+,$$]/ {$$4=dir; print}' <$@ >>../$@;
	    cd ..;
	  fi;
	done;



.META: rnaseq.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered

# add the coverage of the single exons and identify
# those with rpkm > 0 as novel functional
exon.func.win.bed: rnaseq.cov rnaseq.fstats exon.novel.win.bed 
	$(call load_modules);
	cat <<'EOF' | bRscript - | bsort -k 1,1V -k 2,2n >$@
	options(width=10000)
	setwd("$(PWD)")
	
	# standard implementation
	countToTpm = function(counts, mapped, effLen) {
		rpk = counts / effLen
		denom = mapped / 1e6
		return(rpk / denom)
	}
	
	countToFpkm = function(counts, mapped, effLen) {
		return( exp( log(counts) + log(1e9) - log(effLen) - log(mapped) ) )
	}
		
	# rnaseq data	
	cname = c(	"reference", "start", "end", "name", "score", "strand", "attribute", "feature", "frame",
						"description","overlapping_reads","covered_positions","feature_length","feature_covered"	)
	
	rnaseq_data = bread(file="$<", header=F)
	colnames(rnaseq_data) = cname
	
	rnaseq_fstat = bread(file="$^2", header=F, check.names=T)
	rnaseq_mapped = as.integer(rnaseq_fstat[1,4])
	
	novel_data = bread(file="$^3", header=F)
	colnames(novel_data) = cname
	
	merged = merge(novel_data, rnaseq_data, by=c("reference", "start", "end", "feature"), all.x=T)
	
	merged_slice = merged[ , c( "reference", "start", "end", "name.x", "score.x", "strand.x", "attribute.x", "feature", "frame.x",
								"description.x","overlapping_reads.x","covered_positions.x","feature_length.x","feature_covered.x",
								"overlapping_reads.y", "covered_positions.y", "feature_length.y", "feature_covered.y" )]

	
	colnames(merged_slice) = c(cname, "rnaseq_overlapping_reads","rnaseq_covered_positions","rnaseq_feature_length","rnaseq_feature_covered")
	
	rnaseq_counts = as.integer(merged_slice$$rnaseq_overlapping_reads)
	rnaseq_len = merged_slice$$rnaseq_feature_length/1000
	merged_slice$$"rnaseq_rpkm" = with(merged_slice, countToFpkm(rnaseq_counts, rnaseq_mapped, rnaseq_len))
	
	# select novel deleted functional exonsq
	novel_funct = merged_slice[ which( merged_slice$$"rnaseq_rpkm" > 0 ), ]
	novel_funct$$"type" = rep("functional", nrow(novel_funct))
	
	# select novel deleted non-functional exons
	novel_pseudo = merged_slice[ which( merged_slice$$"rnaseq_rpkm" <= 0 ), ]
	novel_pseudo$$"type" = rep("pseudo", nrow(novel_pseudo))
	
	# merge dataframes
	novel = rbind(novel_funct, novel_pseudo)
	
	# sort in increasing order for the "self-pn_l2r" column
	novel = novel[ order(novel$$"rnaseq_rpkm"), ]
	
	bwrite( novel, row.names=F, col.names=F )
	
	EOF




# calculate some statistics about the number of novel exons
exon.novel.win.stat: exon.func.win.bed exon.tot.win.bed exon.tot.scaff.bed
	$(call load_modules); \
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	# take header from file
	novel = bread( file="$<", header=F )
	tot_mapped = bread( file="$^2", header=F )
	tot_map_unmap = bread( file="$^3", header=F )
	
	# select only those marked as pesudoexon
	novel_pseudo = novel[ novel$$V20 == "pseudo", ]
	novel_pseudo_freq = table( novel_pseudo$$V4 )
	novel_pseudo_freq_frame = as.data.frame( novel_pseudo_freq )
	
	# select only those marked as functional
	novel_funct = novel[ novel$$V20 == "functional", ]
	novel_funct_freq = table( novel_funct$$V4 )
	novel_funct_freq_frame = as.data.frame( novel_funct_freq )
	
	tot_mapped_freq = table( tot_mapped$$V4 )
	tot_mapped_frame = as.data.frame( tot_mapped_freq )
	
	tot_map_unmap_freq = table( tot_map_unmap$$V4 )
	tot_map_unmap_frame = as.data.frame( tot_map_unmap_freq )
	
	# change suffices when recursivelly merge dataframe
	merged_frame = merge(	merge( 	merge(tot_map_unmap_frame, 
							tot_mapped_frame, by=1, all.x=T, all.y=T, suffixes=c(".a",".b")),
							novel_pseudo_freq_frame, by=1, all.x=T, all.y=T, suffixes=c(".c",".d")),
							novel_funct_freq_frame, by=1, all.x=T, all.y=T, suffixes=c(".e",".f"))

	colnames(merged_frame) = c("chr","tot. scaff. exons","tot. mapped exons","tot. pseudo exons", "tot. novel exons")
	bwrite( merged_frame[ order(merged_frame[2], decreasing=T ), ], 
			file="$@", row.names=F, col.names=T )
	EOF



.PHONY: test
test:
	@echo

ALL += ref.fa \
	annot.sort.bed \
	subsystem.flag \
	exon.novel.win.stat \
	exon.novel.win.bed \
	exon.tot.win.bed \
	exon.tot.scaff.bed
	

INTERMEDIATE += scaff.fa

CLEAN += dir_lst.mk \
	exon.func.win.bed \
	exon.novel.win.bed \
	exon.novel.win.stat \
	exon.tot.scaff.bed \
	exon.tot.win.bed \
	subsystem.flag

# add dipendence to clean targhet
clean: clean_dir

.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done
