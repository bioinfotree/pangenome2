# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

# size of the window in which the reference will be subdivided
WIN_SIZE ?= 200000
# percentace of windows slop at both sizes
WIN_SLOP ?= 2.5
# if an exon is covered <= of this fraction it's considered novel
EXON_MAX_COV ?= 0.5

# log dir
log:
	mkdir -p $@

# subdivide complete list of scaffolds into two slices to overcome
# list length limitation.
# for each scaffold I report: <scaffold_name>_<chr_span> i.e:
# scaffold_9707_861
# chr_span is the column 9 of filtered.assembly.bam_placed-full.txt from Scappa
windows.mk: scapa.bed
	ITEMS=$$(cut -f4 <$< | wc -l)
	WIN1=$$(bawk 'BEGIN { i=1; } { split($$4,a,"_"); print a[1]"_"i; i++;}' $< | sed -n '1,'"$$(($$ITEMS / 2))"'p' | tr \\n ' ')   * first half of the list *
	WIN2=$$(bawk 'BEGIN { i=1; } { split($$4,a,"_"); print a[1]"_"i; i++;}' <$< | sed -n ''"$$(($$ITEMS / 2 + 1))"',$$p' | tr \\n ' ')   * second half of the list *
	printf "WIN1 := $$WIN1\n" >$@;
	printf "WIN2 := $$WIN2\n" >>$@;

# source the new variable
include windows.mk


WIN_REF1 = $(addsuffix .ref.win.fasta,$(WIN1))
WIN_REF2 = $(addsuffix .ref.win.fasta,$(WIN2))
# for each line in the scapa.bed
# retrieve the mapping portion of a given scaffolds to the reference.
# the size of the alignment is taken as it is.
scaffold_1.ref.win.fasta: scapa.bed genome.tab ref.fa
	$(call load_modules);
	(
	i=1;
	while read p; do		
		FILENAME="$$(echo -e "$$p" | bawk -v i=$$i '{ split($$4,a,"_"); print a[1]"_"i; }').ref.win.fasta"
		echo -e "$$p" \
		| bedtools getfasta -fi $^3 -bed stdin -fo stdout >"$$FILENAME"
		((i++));
	done
	) <$<;
	sleep 3

# fake generation of all the other targets similar to the previous
$(filter-out scaffold_1.ref.win.fasta,$(WIN_REF1) $(WIN_REF2)): scaffold_1.ref.win.fasta
	touch $@





WIN_SCAFF1 = $(addsuffix .scaff.win.fasta,$(WIN1))
WIN_SCAFF2 = $(addsuffix .scaff.win.fasta,$(WIN2))
# for each line in the scapa.bed
# retrieve the full length of the scaffold that map on the
# corresponding scaffold_*.ref.win.fasta
# the size of the alignment is increase by slope at both sides
scaffold_1.scaff.win.fasta: scapa.bed scaff.fa
	$(call load_modules);
	(
	i=1;
	while read p; do
		FILENAME="$$(echo -e "$$p" | bawk -v i=$$i '{ split($$4,a,"_"); print a[1]"_"i; }').scaff.win.fasta"
		echo -e "$$p" \
		| select_columns 4 \
		| xargs samtools faidx $^2 >$$FILENAME;
		((i++));
	done
	) <$<;
	sleep 3

# fake generation of all the other targets similar to the previous
$(filter-out scaffold_1.scaff.win.fasta,$(WIN_SCAFF1) $(WIN_SCAFF2)): scaffold_1.scaff.win.fasta
	touch $@






WIN_BAM1 = $(addsuffix .ref.win.bam,$(WIN1))
WIN_BAM2 = $(addsuffix .ref.win.bam,$(WIN2))
# align each referencce sequence belonging to a window to the corresponding scaffolds 
# in that window with denom aligner.
# I generate fragments from the long reference sequence 10kb long and overlapping by 1kb 
# Here I remove the range of coordinates from scaffold chunk names, generate by faidx (10359-10459)
# otherwise this create problemas with intersection with annotations.
%.ref.win.bam: %.ref.win.fasta %.scaff.win.fasta log
	echo -e "process for chunk $*";
	module purge;
	module load aligners/imrdenom/0.4.1;
	module load sw/bio/samtools/0.1.19;
	module load sw/bio/emboss/6.4.0;
	module load lang/python/2.7;
	mkdir -p $*;
	cd $*;
	ln -sf ../$< .;
	ln -sf ../$^2 .;
	if [ -s $< ] && [ -s $^2 ]; then
		fasta2tab <$< \
		| bawk '{ split($$1,a,":"); print a[1],$$2 }' \   * remove :10359-10459 from fasta headers; this make denom crush *
		| bsort \
		| tab2fasta 2 >ref.fasta;   * generate new local referece *
		/usr/bin/time -v \   * retain statistics about denom operations *
		error_ignore -- \   * avoid denom failure *
		denom easyrun ref.fasta \
		<(fasta2tab <$^2 \
		| sed 's/:/\_/' \   * convert ":" in fasta header to "_" *
		| tab2fasta -s 2) \
		$@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log
		
		if [ -s $@ ]; then
			mv $@ ../;
		fi;
		
		if [ -s $(basename $@).sdi ]; then
			mv $(basename $@).sdi ../;
		fi;
		
	fi;
	cd ..;
	rm -r $*;

WIN_SDI1 = $(addsuffix .ref.win.sdi,$(WIN1))
WIN_SDI2 = $(addsuffix .ref.win.sdi,$(WIN2))
# variats calculated by denom
%.ref.win.sdi: %.ref.win.bam
	touch $@


WIN_ANNOT1 = $(addsuffix .exon.tot.win.bed,$(WIN1))
WIN_ANNOT2 = $(addsuffix .exon.tot.win.bed,$(WIN2))
# Select annotations that are completelly (100%) within the part of the scaffolds that
# are mapped to the referece
%.exon.tot.win.bed: annot.sort.bed %.ref.win.fasta
	$(call load_modules);
	if [ -s $^2 ]; then
		bedtools intersect \
		-bed \   * write output as BED, not bam *
		-f 1.0 \
		-wa \
		-a <(bawk '!/^[\#+,$$]/ { if ( $$8 ~ "exon" ) print $$0 }' $<) \   * select only exons *
		-b <(fasta2tab <$^2 | cut -f1 | tr ':-' \\t) \
		| sortBed -i stdin >$@;
	fi


####### CHANGE COORDINATES
# denom is used to align the scaffold to the corresponding reference windows so the coordinates of the
# aligned sequences are from 0 to the length of the reference windows.
# Instead the coordinates of the exons in the reference windows are with respect to the entire chromosome.
# Here I convert the coordinates of the exons from chr -> local windows.
# Then I check the exons for coverage of the corresponding scaffold
# Than I convert back the the coordinates of the exons from local windows -> chr

# prepare chain file: from chr span to windows span
%.chain: genome.tab %.ref.win.fasta
	fasta2tab <$^2 \
	| cut -f1 \
	| tr ':-' \\t \
	| translate -a $< 1 \
	| bawk 'BEGIN { N=0; } \
	!/^[\#+,$$]/ { \
	N++; \
	len=$$4-$$3;
	print "chain 10 "$$1" "$$2" + "$$3" "$$4" "$$1" "len" + "0" "len" "N; \
	print len; \
	print ""; \
	} ' >$@

# referse the chain
%.rew.chain: %.chain
	$(call load_modules);
	chainSwap $< $@

# liftover the coordinates of all exons within the referece window
%.lift.exon.tot.win.bed: %.chain %.exon.tot.win.bed
	$(call load_modules);
	CrossMap.py bed $< $^2 $@




.META: %.exon.novel.win.bed
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered

WIN_EXON1 = $(addsuffix .exon.novel.win.bed,$(WIN1))
WIN_EXON2 = $(addsuffix .exon.novel.win.bed,$(WIN2))
# Select exons that are covered <= $(EXON_MAX_COV) by scaffold
%.lift.exon.novel.win.bed: %.lift.exon.tot.win.bed %.ref.win.bam
	$(call load_modules);
	if [ -s $< ] && [ -s $^2 ]; then
		bedtools coverage \
		-abam $^2 \
		-b $< \
		| bawk '!/^[\#+,$$]/ { if ( $$14 <= $(EXON_MAX_COV) && $$8 ~ "exon" ) print $$0 }' >$@;
	fi


####### CHANGE BACK

# liftover beck the coordinates of all exons within the referece window
# I need to sobstitute the separator  with "!" in order to be able to convert the entire bed with crossmap
# than I change it back to tab
WIN_EXON1 = $(addsuffix .exon.novel.win.bed,$(WIN1))
WIN_EXON2 = $(addsuffix .exon.novel.win.bed,$(WIN2))
%.exon.novel.win.bed: %.rew.chain %.lift.exon.novel.win.bed
	$(call load_modules);
	if [ -s $^2 ]; then
		SEP="!";
		CrossMap.py bed $< \
		<(awk -v sep="$$SEP" -F'\t' '{ \
		printf "%s\t%s\t%s\t", $$1, $$2, $$3; \
		for ( i=4; i<NF; i++ ) { \
			printf "%s%s", $$i, sep; } \
			printf "%s\n", $$NF; }' \
		$^2) \
		| cut -f 6-9 \
		| sed -e '/^$$/d' -e 's/'"$$SEP"'/\t/g' \
		| bsort -k1,1 -k2,2n >$@;
	fi


####### CHANGE COORDINATES


# collect all novel exons
# thare could be some redundancy about the exons found
# since the scaffolds are mapped to the reference through overlapping windows
exon.novel.win.bed: $(WIN_EXON1) $(WIN_EXON2)
	(
	for F in $^; do
		if [ -s $$F ]; then
			cat $$F
		fi;
	done
	) \
	| bsort -k1,1 -k2,2n \
	| uniq >$@

# select all the exons that are present in the portion of the scaffolds
# mapped to the reference
exon.tot.win.bed: $(WIN_ANNOT1) $(WIN_ANNOT2)
	(
	for F in $^; do
		if [ -s $$F ]; then
			bawk '!/^[\#+,$$]/ { if ( $$8 ~ "exon" ) print $$0 }' $$F
		fi;
	done
	) \
	| bsort -k1,1 -k2,2n \
	| uniq >$@


.PHONY: test
test:
	rm $(WIN_SDI1) $(WIN_SDI2)

ALL += ref.fa \
	exon.novel.win.bed \
	exon.tot.win.bed

INTERMEDIATE += ref.windows.bed \
	$(WIN_REF1) $(WIN_REF2) \
	$(WIN_BAM1) $(WIN_BAM2) \
	$(WIN_EXON1) $(WIN_EXON2) \
	$(WIN_SDI1) $(WIN_SDI2) \
	$(WIN_SCAFF1) $(WIN_SCAFF2)
	
	

CLEAN += scaff.fa \
	genome.tab
